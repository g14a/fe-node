import React, {Component } from 'react';
import jwt_decode from 'jwt-decode';

import {deleteAccount, editAccount} from './UserFunctions'

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            name: '',
            email: '',
            inputEdit: false
        }
    }

    componentDidMount() {
        const token = localStorage.jsontoken
        const decoded = jwt_decode(token)

        this.setState({
            name: decoded.name,
            email: decoded.email
        })
    }

    onClickDelete() {
        deleteAccount()
        .then(res => {
            if(res) {
                this.props.history.push('/register')
            }
        })
    }

    editEnable =() => {
        this.setState({
            inputEdit: true
        })
    }

    handleEmailChange = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    handleEmailSave = () => {

        const token = localStorage.jsontoken
        const decoded = jwt_decode(token)

        const user = {
            name: decoded.name,
            email: this.state.email
        }

        editAccount(user).then(res => {
            if(res) {
                this.setState({
                    inputEdit: false
                })
            }
        })
        // do this after fetch
    }

    render() {
        return(
            <div className="container">
                <div className="jumbotron mt-5">
                    <div className="col-sm-8 mx-auto">
                        <h1 className="text-center">PROFILE</h1>
                    </div>
                    <table className="table col-md-6 mx-auto">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{this.state.name}</td>
                            </tr>
                            {!this.state.inputEdit && 
                            <tr>
                                <td>Email</td>
                                <td>{this.state.email}</td>
                                <button onClick={this.editEnable}>Edit</button>
                            </tr>
                            }
                            {this.state.inputEdit && 
                            <tr>
                                <td>Email</td>
                                <td>
                                    <input value={this.state.email} onChange={this.handleEmailChange} />
                                </td>
                                <button onClick={this.handleEmailSave}>Save</button>
                            </tr>
                            }
                            <tr>
                                <td>&nbsp;&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="col-sm-2 mx-auto">
                    <button className="btn btn-lg btn-space btn-primary btn-block" onClick={() => this.props.history.push(`/login`)}>
                                Sign Out
                    </button>
                    </div>
                </div> 
                <div className="col-sm-3 mx-auto">
                    <button className="btn btn-lg btn-space btn-outline-danger btn-block" onClick={() => this.onClickDelete()}>
                                    Delete Account
                    </button>
                </div>
            </div>
        )
    }
}

export default Profile