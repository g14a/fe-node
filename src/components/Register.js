import React, {Component } from 'react';
import FlashMessage from 'react-flash-message';
import {register, authGoogle} from './UserFunctions';

class Register extends Component {
    constructor() {
        super()
        this.state = {
            name: '',
            email: '',
            password: '',
            flashMessage: () => {
                return (
                    <FlashMessage duration={5000}>
                    <strong>Succesfully signed up</strong>
                    </FlashMessage>
                )   
            }
        }
       this.onChange = this.onChange.bind(this)
       this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    onGoogleAuth() {
        authGoogle()
        .then(res => {
            if(res) {
                this.setState({
                    googleResponseHTML: res.text()
                })
                this.props.history.push(`/auth/google`)
            }
        })
    }

    onSubmit(e) {
        e.preventDefault()

        const User = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }

        register(User).then(res => {
            if(res)
                this.props.history.push(`/login`)
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal"> Please Register</h1>
                            
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input type="name"
                                className="form-control"
                                name="name"
                                placeholder="Enter Name"
                                value={this.state.name}
                                onChange={this.onChange}/>
                            </div>
                            
                            <div className="form-group">
                                <label htmlFor="email">Email Address</label>
                                <input type="email"
                                className="form-control"
                                name="email"
                                placeholder="Enter Email"
                                value={this.state.email}
                                onChange={this.onChange}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="Password"
                                className="form-control"
                                name="password"
                                placeholder="Enter Password"
                                value={this.state.password}
                                onChange={this.onChange}/>
                            </div>
                            <button className="btn btn-lg btn-primary btn-block" onClick={() => this.state.flashMessage()}>
                                Register
                            </button>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                        </form>
                        <button className="btn btn-google btn-lg btn-primary btn-block" onClick={() => this.onGoogleAuth()}>
                                Google
                            </button>
                            <div>&nbsp;</div>

                    </div>
                </div>
            </div>
        )
    }
}

export default Register