import { rp } from 'request-promise';
import accessToken from '../config/config';

let herokuURL = "https://node-scapic.herokuapp.com"
let localURL = "http://localhost:8000"

export const register = (newUser) => {
    return fetch(localURL + '/register', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            name: newUser.name,
            email: newUser.email,
            password: newUser.password
        }),
    })
    .then(res => res.json())
}

export const login = (user) => {

    return fetch(localURL + '/login', {
        method: "POST",
        mode: 'cors',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: user.email,
            password: user.password
        }),

    }).then(res => res.json())
    .then(resJson => {
        localStorage.setItem('jsontoken', resJson.token)
        return resJson.token
    })
    .catch(err => {
        console.log(err)
    })
}

export const deleteAccount = () => {
    return fetch(localURL + '/profile', {
        method: "DELETE",
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('jsontoken')
        },
    }).then(res => res.json())
    .then(resJson => {
        return resJson
    })
    .catch(err => {
        console.log(err)
    })
}  

export const authGoogle = () => {
    return fetch(herokuURL + '/auth/google', {
        method: "POST",
        mode: 'cors',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify ({
            access_token : accessToken
        })
    }).then(res => {
        console.log(res)
        return res
    })
    .catch(err => {
        console.log(err)
    })
}

export const editAccount = (user) => {
    return fetch(localURL + '/profile/' + user.name, {
        method: "PUT",
        mode: 'cors',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('jsontoken')
        },
        body: JSON.stringify({
            email: user.email,
        })
    }).then(res => {
        return res
    }).catch(err => {
        console.log(err)
    })
}